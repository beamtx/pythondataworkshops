# Workshop 1: Getting Started

## Prerequisites

* `python3` installed on your computer. `python --version` executed in a terminal should be a number >= 3.7 (although any 3.x version should suffice). For Mac, [this guide is good.](https://wsvincent.com/install-python3-mac/)

## Step 0: Navigating to your project folder in Terminal

1. Open the Terminal app.
2. `cd ~/Documents` changes your current directory (where your command prompt "is") to the `Documents` folder
3. `mkdir pythonworkshops` creates a directory (folder) called `"pythonworkshops"`
4. `cd pythonworkshops` changes your directory to `pythonworkshops`

Now your terminal prompt is inside the `pythonworkshops` folder!

## Step 1: Setting up your virtual environment

We use the term `environment`, more specifically "virtual environment", to describe the software that your project is able to use. As a beginner, you often will be using the same software for all of your projects, but as you advance, you will start to use different tools for different jobs. A *defined* environment allows you to specify what (software) tools are required, similar to the materials portions of a materials and methods section.

### Create and activate a python virtual environment

Make sure you have completed Step 0. If you are unsure whether your command prompt is in the correct place, type `pwd` and hit enter. This gives you the current location of your command prompt. Make sure it is the project folder you want.

1. `python3 -m venv env` This command uses the python3 module `venv` to create a virtual environment in a folder titled `env` in the current directory. This virtual environment will keep all of your stuff together and isolated from other projects.
2. `source env/bin/activate` This runs the command in `env/bin/activate` to "activate" the virtual environment. You should see `(env)` at the beginning of your command prompt, and when you type `which python` it should have a path within the `env` directory.

Under the hood, you have just created an "alias" for `python3` located at `env/bin/python` and changed the command `python` to reference that alias. When you deactivate the environment with `deactivate` you leave the virtual environment.

## Step 2: Install the required "packages"

Python packages (also called modules) are Python code that can be installed and used in your own projects. Although you could copy and paste the third-party code into your project's folder, keeping track of versions and dependencies would get very complicated. "Package managers" exist to solve this problem. PyPI, the Python Package Index, is a central repository (website) containing packages that can be installed into your project. There are other package repositories as well; you may have heard of Conda or Anaconda, for example. 

To install packages in Python, the most common tool is [pip](https://pip.pypa.io/en/stable/quickstart/). Let's get familiar with `pip` by first having it upgrade itself.

Run `pip install --upgrade pip`. You should see some logs that say something like "Uninstalling pip-..." "Successfully installed pip-20...". This command used `pip` to `install` the package `pip` and `--upgrade` the existing (old) `pip` package.

The general syntax for using `pip` is: `pip install {package_name} {package_name2} {package_name3}`, you can include one package at a time, or multiple packages. When you install a package with `pip` it will also install any dependencies (packages required), so a single package install could download dozens of Python packages.

### Let's install two packages that we need

`pip install pandas jupyter` A bunch of things will be installed (`pandas`, `jupyter`, and their dependencies). The command should end with "Successfully installed..." and a list of packages and versions.

Let's save the list of packages we just installed (good practice) to a text file, so we can create this environment on another computer. Run `pip freeze > requirements.txt`. This command saves the list of packages and their versions into the text file `requirements.txt`. If you every need to install these packages from this file again use `pip install -r requirements.txt`.

## Step 3: Open a Jupyter Notebook

There are many ways to run, write, and build python code. Jupyter notebooks are one of the most common ways to write python interactively so you can quickly iterrate through your mistakes. They also have builtin capabilities for viewing tables and data visualization.

1. Run `jupyter notebook`. A browser window should open and the url should be something like "http://localhost:8888/tree". This is NOT a website, it is an application on your computer that YOU are running from your terminal when you type `jupyter notebook`. The browser (Chrome or otherwise) is just the way you interact with this application.
2. In the upper right, click "New" "Notebook: Python 3".
3. Click "Untitled.ipynb" in the file browser to open the notebook (ipynb stands for interactive python notebook)
4. Rename the notebook by clicking at the top on "Untitled". Name it something new and descriptive.
5. Click in the box next to "In [ ]:". This area is called a "cell" and you can write python code into it.
6. Type `print("Hello Beam")` into the box and hit shift+enter on your keyboard to execute the cell

You should see "Hello Beam" as an output. Congratulations, you've written your first bit of python in a notebook.

Feel free to play around with writing code if you know anything about python and want to try it out here.

## Step 4: Load the example data with pandas

[`pandas` is a python package](https://pandas.pydata.org/docs/) for dealing with tabular data. It has become the defacto standard for data science. The syntax for working with `pandas` is a little different than "normal" python, so I'm not even going to try and cover the basics of "normal python" for now.

To use the `pandas` package in your python code, you must "import" it.

1. In your first cell in your notebook type `import pandas as pd` and hit shift+enter to run the cell.
2. In your second cell type `df = pd.read_csv("run_summary.csv")` and hit shift+enter. If this runs successfully, you have loaded the data into a variable called `df`, short for DataFrame. This variable can be called anything you want (without spaces or weird characters), but the convention for toy examples is to call it `df` until you come up with a better name.
3. In your third cell type `df` and hit shift+enter. The output should show an abbreviated view of the tabular data. At the bottom, it will tell you the "shape" of the data, how many rows and columns there are.

## Basic commands with pandas

* List the columns in the dataframe: `df.columns`
* Get the shape of the dataframe: `df.shape`
* Get a specific column of the dataframe: `df["insert column name here"]` or `df.column_name` if the column_name does not have spaces and doesn't overlap a reserved word.
* Add two columns: `df["sum_column"] = df["column1"] + df["column2"]`, same works for multiply (`*`), divide (`/`), subtract (`-`).
* Sum of a column: `df["sum"] = df["column_name"].sum()`
* Mean of a column: `df["mean"] = df["column_name"].mean()`
* Select a list of columns: `df2 = df[["col1", "col2", "col3"]]`
* Write a DataFrame to a file `df.to_csv("filepath/file_name.csv")`
* Convert datatypes: `df["int_as_string"] = df["int_column"].astype(str)`

## Intermediate commands with pandas

* "Mask" a certain condition, e.g. values > 0.5: `high_value_mask = df["column_name"] > 0.5`
* Select with logic: 
```python
high_value_mask = df["column_name"] > 0.5
high_value_rows = df.loc[high_value_mask, :]
```

## Python syntax

You don't need to know everything about python basics to navigate data with pandas, but you do need some knowledge of basic syntax. Here's a very brief, incomplete cheatsheet for some basics.

### Objects and their types

Python objects (variables, functions, etc) all have a type. 

#### The most common "basic" types are:

* `int`: integer, for whole valued numbers
* `str`: string, for text
* `float`: decimal number, for numbers that require decimals

#### For collecting values together:

* `list`: list, a list of objects. Defined with `my_list_of_ints = [1, 2, 3]`.
* `dict`: dictionary, a "mapping" of objects. Given one object as a "key" it will return another as a "value". Defined with `my_dict_mapping_str_to_int = {"a": 1, "b": 2}`
* `tuple`: tuple, an iterable of objects. Similar to a list, except you cannot change the contents once it has been defined (it is not a "mutable" type). Defined with `my_tuple_of_ints = (1, 2, 3)`

#### Functions

Functions do stuff. They are defined with `def`.

```python
def my_function_to_add_five(input_integer):
    return input_integer + 5
```

